﻿
DECLARE @RegionsSnapshot TABLE (
	[Id] [int] NOT NULL,
	[ParentRegionId] [int] NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsDeleted] [bit] NOT NULL
)

INSERT INTO @RegionsSnapshot 
VALUES 
(1, NULL, 'Root', 0),
(2, 1, 'Geoscope Solutions Pty Ltd', 0),
(3, 2, 'Australia', 0),
(4, 3, 'New South Wales', 0),
(5, 4, 'Haberfield', 0)

INSERT INTO 
	[quartet].[Regions]
           ([Id]
		   ,[ParentRegionId]
           ,[Name]
           ,[IsDeleted])
SELECT
	s.Id, s.ParentRegionId, s.Name, s.IsDeleted
FROM 
	@RegionsSnapshot s
	LEFT JOIN [quartet].[Regions] dde ON dde.Id = s.Id
WHERE dde.Id IS NULL

UPDATE dde
SET
	dde.Id = s.Id,
	dde.ParentRegionId = s.ParentRegionId,
	dde.Name = s.Name,
	dde.IsDeleted = s.IsDeleted
FROM 
	@RegionsSnapshot s 
	INNER JOIN [quartet].[Regions] dde ON dde.Id = s.Id

DELETE dde
FROM
	@RegionsSnapshot s
	RIGHT JOIN [quartet].[Regions] dde ON dde.Id = s.Id
WHERE
	s.Id IS NULL
