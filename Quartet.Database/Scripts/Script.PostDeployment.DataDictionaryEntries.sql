﻿
DECLARE @DataDictionaryEntriesSnapshot TABLE (
	[Id] [int] NOT NULL,
	[RegionId] [int] NULL,
	[Category] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ViewOrder] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL
)

INSERT INTO @DataDictionaryEntriesSnapshot 
VALUES 
(1, NULL, 'CompanyType', 'Customer', 10, 0),
(2, NULL, 'CompanyType', 'Supplier', 10, 0),
(3, NULL, 'CompanyType', 'Internal', 10, 0)

INSERT INTO 
	[quartet].[DataDictionaryEntries]
           ([Id]
		   ,[RegionId]
           ,[Category]
           ,[Name]
           ,[ViewOrder]
           ,[IsDeleted])
SELECT
	s.Id, s.RegionId, s.Category, s.Name, s.ViewOrder, s.IsDeleted
FROM 
	@DataDictionaryEntriesSnapshot s
	LEFT JOIN [quartet].[DataDictionaryEntries] dde ON dde.Id = s.Id
WHERE dde.Id IS NULL

UPDATE dde
SET
	dde.Id = s.Id,
	dde.RegionId = s.RegionId,
	dde.Category = s.Category,
	dde.Name = s.Name,
	dde.ViewOrder = s.ViewOrder,
	dde.IsDeleted = s.IsDeleted
FROM 
	@DataDictionaryEntriesSnapshot s 
	INNER JOIN [quartet].[DataDictionaryEntries] dde ON dde.Id = s.Id

DELETE dde
FROM
	@DataDictionaryEntriesSnapshot s
	RIGHT JOIN [quartet].[DataDictionaryEntries] dde ON dde.Id = s.Id
WHERE
	s.Id IS NULL
