﻿
DECLARE @SystemCompaniesSnapshot TABLE (
	[Id]             INT NOT NULL,
	[RegionId]		 INT NOT NULL,
    [Name]           NVARCHAR (255) NOT NULL,
	[IsEnabled]      BIT NOT NULL,
	[IsDeleted]      BIT NOT NULL,
    [CreatedDate]    DATETIME2 (3)  NOT NULL,
    [CreatedBy]      INT            NOT NULL,
    [LastModified]   DATETIME2 (3)  NOT NULL,
    [LastModifiedBy] INT            NOT NULL
)

INSERT INTO @SystemCompaniesSnapshot 
VALUES 
(1, 2, 'Geoscope Solutions Pty Ltd', 1, 0, '2016-01-01 09:00:00', 1, '2016-01-01 09:00:00', 1)

INSERT INTO 
	[quartet].[SystemCompanies]
           ([Id]
		   ,[RegionId]
		   ,[Name]
		   ,[IsEnabled]
		   ,[IsDeleted]
           ,[CreatedDate]
           ,[CreatedBy]
		   ,[LastModified]
           ,[LastModifiedBy])
SELECT
	s.Id, s.RegionId, s.Name, s.IsEnabled, s.IsDeleted, s.CreatedDate, s.CreatedBy, s.LastModified, s.LastModifiedBy
FROM 
	@SystemCompaniesSnapshot s
	LEFT JOIN [quartet].[SystemCompanies] dde ON dde.Id = s.Id
WHERE dde.Id IS NULL

UPDATE dde
SET
	dde.Id = s.Id,
	dde.RegionId = s.RegionId,
	dde.Name = s.Name,
	dde.IsEnabled = s.IsEnabled,
	dde.IsDeleted = s.IsDeleted,
	dde.CreatedDate = s.CreatedDate,
	dde.CreatedBy = s.CreatedBy,
	dde.LastModified = s.LastModified,
	dde.LastModifiedBy = s.LastModifiedBy
FROM 
	@SystemCompaniesSnapshot s 
	INNER JOIN [quartet].[SystemCompanies] dde ON dde.Id = s.Id

DELETE dde
FROM
	@SystemCompaniesSnapshot s
	RIGHT JOIN [quartet].[SystemCompanies] dde ON dde.Id = s.Id
WHERE
	s.Id IS NULL
