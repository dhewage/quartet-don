﻿CREATE TABLE [quartet].[Regions]
(
	[Id] INT NOT NULL PRIMARY KEY, 
	[ParentRegionId] INT NULL,
    [Name] NVARCHAR(255) NOT NULL,
	[IsDeleted] BIT NOT NULL DEFAULT 0 
)
