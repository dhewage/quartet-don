﻿CREATE TABLE [quartet].[SystemCompanies] (
    [Id]             INT NOT NULL,
	[RegionId]       INT NOT NULL, 
    [Name]           NVARCHAR (255) NOT NULL,
	[IsEnabled]      BIT NOT NULL DEFAULT 1,
	[IsDeleted]      BIT NOT NULL DEFAULT 0,
    [CreatedDate]    DATETIME2 (3)  NOT NULL,
    [CreatedBy]      INT            NOT NULL,
    [LastModified]   DATETIME2 (3)  NOT NULL,
    [LastModifiedBy] INT            NOT NULL,
    CONSTRAINT [PK_SystemCompanies] PRIMARY KEY CLUSTERED ([Id] ASC)
);

