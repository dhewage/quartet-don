﻿CREATE TABLE [quartet].[DataDictionaryEntries]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [RegionId] INT NULL, 
    [Category] NVARCHAR(50) NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL, 
    [ViewOrder] INT NOT NULL DEFAULT 0, 
    [IsDeleted] BIT NOT NULL DEFAULT 0
)
