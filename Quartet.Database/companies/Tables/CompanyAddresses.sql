﻿CREATE TABLE [companies].[CompanyAddresses] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
    [CustomerId]     INT            NOT NULL,
    [IsDefault]      BIT            CONSTRAINT [DF_CompanyAddresses_IsDefault] DEFAULT ((0)) NOT NULL,
    [Name]           NVARCHAR (255) NOT NULL,
    [Street1]        NVARCHAR (255) NULL,
    [Street2]        NVARCHAR (255) NULL,
    [Street3]        NVARCHAR (255) NULL,
    [City]           NVARCHAR (255) NULL,
    [State]          NVARCHAR (10)  NULL,
    [Postcode]       NVARCHAR (10)  NULL,
    [CountryId]      INT            NULL,
    [IsDeleted]      BIT            CONSTRAINT [DF_CompanyAddresses_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedDate]    DATETIME2 (3)  NOT NULL,
    [CreatedBy]      INT            NOT NULL,
    [LastModified]   DATETIME2 (3)  NOT NULL,
    [LastModifiedBy] INT            NOT NULL,
    CONSTRAINT [PK_CompanyAddresses] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CompanyAddresses_Customers] FOREIGN KEY ([CustomerId]) REFERENCES [companies].[Companies] ([Id])
);

