﻿CREATE TABLE [companies].[Companies] (
    [Id]             INT            IDENTITY (1, 1) NOT NULL,
	[RegionId] INT NOT NULL,
	[CompanyTypeId] INT NOT NULL,
    [Name]           NVARCHAR (255) NOT NULL,
    [IsEnabled]      BIT            CONSTRAINT [DF_Customers_IsEnabled] DEFAULT ((1)) NOT NULL,
    [IsDeleted]      BIT            CONSTRAINT [DF_Customers_IsDeleted] DEFAULT ((0)) NOT NULL,
    [CreatedDate]    DATETIME2 (3)  NOT NULL,
    [CreatedBy]      INT            NOT NULL,
    [LastModified]   DATETIME2 (3)  NOT NULL,
    [LastModifiedBy] INT            NOT NULL,
    CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED ([Id] ASC)
);

