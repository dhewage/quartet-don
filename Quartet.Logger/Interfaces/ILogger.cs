﻿using System;

namespace Quartet.Logger.Interfaces
{
    public interface ILogger
    {
        void Info(string message);

        void Info(string fmt, params object[] vars);

        void Info(Exception exception, string fmt, params object[] vars);

        void Warn(string message);

        void Warn(string fmt, params object[] vars);

        void Warn(Exception exception, string fmt, params object[] vars);

        void Error(string message);

        void Error(string fmt=null, params object[] vars);

        void Error(Exception exception, string fmt=null, params object[] vars);

        void WriteTrace(string componentName, string method, TimeSpan timespan);

        void WriteTrace(string componentName, string method, TimeSpan timespan, string properties);

        void WriteTrace(string componentName, string method, TimeSpan timespan, string fmt, params object[] vars);
    }
}