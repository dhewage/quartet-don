﻿using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Quartet.Logger.Interfaces;
using Quartet.Logger.Utilities;
using System;

namespace Quartet.Logger
{
    public class TelemetryClientLogger : ILogger
    {
        private readonly TelemetryClient telemetryClient = new TelemetryClient();

        ~TelemetryClientLogger()
        {
            this.telemetryClient.Flush();
        }

        public void Info(string message)
        {
            telemetryClient.TrackTrace(message, SeverityLevel.Information);
        }

        public void Info(string fmt, params object[] vars)
        {
            telemetryClient.TrackTrace(string.Format(fmt, vars), SeverityLevel.Information);
        }

        public void Info(Exception exception, string fmt, params object[] vars)
        {
            var telemetry = new TraceTelemetry(string.Format(fmt, vars), SeverityLevel.Information);
            telemetry.Properties.Add("Exception", ExceptionUtils.FormatException(exception, includeContext: true));

            telemetryClient.TrackTrace(telemetry);
        }

        public void Warn(string message)
        {
            telemetryClient.TrackTrace(message, SeverityLevel.Warning);
        }

        public void Warn(string fmt, params object[] vars)
        {
            telemetryClient.TrackTrace(string.Format(fmt, vars), SeverityLevel.Warning);
        }

        public void Warn(Exception exception, string fmt, params object[] vars)
        {
            var telemetry = new TraceTelemetry(string.Format(fmt, vars), SeverityLevel.Warning);
            telemetry.Properties.Add("Exception", ExceptionUtils.FormatException(exception, includeContext: true));

            telemetryClient.TrackTrace(telemetry);
        }

        public void Error(string message)
        {
            telemetryClient.TrackTrace(message, SeverityLevel.Error);
        }

        public void Error(string fmt = null, params object[] vars)
        {
            if (fmt != null) telemetryClient.TrackTrace(string.Format(fmt, vars), SeverityLevel.Error);
        }

        public void Error(Exception exception, string fmt = null, params object[] vars)
        {
            var telemetry = new ExceptionTelemetry(exception);
            if (!string.IsNullOrEmpty(fmt)) telemetry.Properties.Add("message", string.Format(fmt, vars));

            telemetryClient.TrackException(telemetry);
        }

        public void WriteTrace(string componentName, string method, TimeSpan timespan)
        {
            WriteTrace(componentName, method, timespan, string.Empty);
        }

        public void WriteTrace(string componentName, string method, TimeSpan timespan, string properties)
        {
            var telemetry = new TraceTelemetry("Trace component call", SeverityLevel.Verbose);
            telemetry.Properties.Add("component", componentName);
            telemetry.Properties.Add("method", method);
            telemetry.Properties.Add("timespan", timespan.ToString());

            if (!string.IsNullOrWhiteSpace(properties))
                telemetry.Properties.Add("properties", properties);

            telemetryClient.TrackTrace(telemetry);
        }

        public void WriteTrace(string componentName, string method, TimeSpan timespan, string fmt, params object[] vars)
        {
            WriteTrace(componentName, method, timespan, string.Format(fmt, vars));
        }

        public void TrackRequest(RequestTelemetry rq)
        {
            telemetryClient.TrackRequest(rq);
        }
    }
}