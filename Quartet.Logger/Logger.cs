﻿using Quartet.Logger.Interfaces;
using Quartet.Logger.Utilities;
using System;
using System.Diagnostics;

namespace Quartet.Logger
{
    public class Logger : ILogger
    {
        public void Info(string message)
        {
            Trace.TraceInformation(message);
        }

        public void Info(string fmt, params object[] vars)
        {
            Trace.TraceInformation(fmt, vars);
        }

        public void Info(Exception exception, string fmt, params object[] vars)
        {
            var msg = String.Format(fmt, vars);
            Trace.TraceInformation(string.Format(fmt, vars) + ";Exception Details={0}", ExceptionUtils.FormatException(exception, includeContext: true));
        }

        public void Warn(string message)
        {
            Trace.TraceWarning(message);
        }

        public void Warn(string fmt, params object[] vars)
        {
            Trace.TraceWarning(fmt, vars);
        }

        public void Warn(Exception exception, string fmt, params object[] vars)
        {
            var msg = String.Format(fmt, vars);
            Trace.TraceWarning(string.Format(fmt, vars) + ";Exception Details={0}", ExceptionUtils.FormatException(exception, includeContext: true));
        }

        public void Error(string message)
        {
            Trace.TraceError(message);
        }

        public void Error(string fmt, params object[] vars)
        {
            Trace.TraceError(fmt, vars);
        }

        public void Error(Exception exception, string fmt, params object[] vars)
        {
            var msg = String.Format(fmt, vars);
            Trace.TraceError(string.Format(fmt, vars) + ";Exception Details={0}", ExceptionUtils.FormatException(exception, includeContext: true));
        }

        public void WriteTrace(string componentName, string method, TimeSpan timespan)
        {
            WriteTrace(componentName, method, timespan, "");
        }

        public void WriteTrace(string componentName, string method, TimeSpan timespan, string fmt, params object[] vars)
        {
            WriteTrace(componentName, method, timespan, string.Format(fmt, vars));
        }

        public void WriteTrace(string componentName, string method, TimeSpan timespan, string properties)
        {
            string message = String.Concat("component:", componentName, ";method:", method, ";timespan:", timespan.ToString(), ";properties:", properties);

            Trace.TraceWarning(message);
        }
    }
}