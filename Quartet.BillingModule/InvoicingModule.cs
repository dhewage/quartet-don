﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quartet.BillingModule
{
    public class InvoicingModule : NancyModule
    {
        public InvoicingModule() : base("/invoicing")
        {
            Get["/"] = _ => {
                return "This is invoicing home";
            };

            Get["/test"] = _ => {
                return "This is invoicing test";
            };
        }
    }
}
