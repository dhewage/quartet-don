﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace Quartet.Core.Models
{
    public interface IRegion
    {
        [Key]
        int Id { get; set; }

        int? ParentRegionId { get; set; }
        string Name { get; set; }
    }

    [Table("quartet.Regions")]
    public class Region : IRegion
    {
        [Key]
        public int Id { get; set; }

        public int? ParentRegionId { get; set; }
        public string Name { get; set; }
    }
}
