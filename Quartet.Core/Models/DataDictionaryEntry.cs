﻿using System.Security.Cryptography.X509Certificates;
using Dapper.Contrib.Extensions;

namespace Quartet.Core.Models
{
    public interface IDataDictionaryEntry
    {
        [Key]
        int Id { get; set; }
        
        int? RegionId { get; set; }
        string Category { get; set; }
        string Name { get; set; }
        int ViewOrder { get; set; }
        bool IsDeleted { get; set; }

    }

    [Table("quartet.DataDictionaryEntries")]
    public class DataDictionaryEntry : IDataDictionaryEntry
    {
        [Key]
        public int Id { get; set; }

        public int? RegionId { get; set; }
        public string Category { get; set; }
        public string Name { get; set; }
        public int ViewOrder { get; set; }
        public bool IsDeleted { get; set; }
    }
}
