﻿using System.Collections.Generic;
using System.Threading.Tasks;
 

namespace Quartet.Core.Interfaces
{
    public interface IQuartetDataRepository<T>
    {
        Task<T> GetById(int Id);
        Task<IEnumerable<T>> GetAll();
        Task<IEnumerable<T>> Find(string sql, object param = null);
        Task<long> Create(T record);
        Task<bool> Update(T record);
        Task<bool> Delete(T record);
        Task<int> SoftDelete(int Id);
    }
    
}