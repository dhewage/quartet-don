﻿Quartet - Companies Module
==========================

This module manages companies and their associated data, such as addresses, notes and more. 
A company can be a customer, supplier / vendor or other, the type is defined in the Quartet
data dictionary.


(C) Copyright 2015 Geoscope Solutions Pty Ltd. All rights reserved, worldwide.
