﻿using Nancy;
using Nancy.ModelBinding;
using Quartet.Companies.Interfaces;
using Quartet.Companies.Models;
using Quartet.Companies.Repositories;
using Quartet.Logger.Interfaces;
using System;

namespace Quartet.Companies.Modules
{
    public class CompanyModule : NancyModule
    {
        private readonly QuartetCompanyRepository _repo;

        public CompanyModule(IQuartetCompanyApplicationSettings appSettings, ILogger _log) : base("/api/companies")
        {
            try
            {
                _log.Info("Entered CompanyModule");

                _repo = new QuartetCompanyRepository(appSettings.DBConnectionString);

                _log.Info("Created  QuartetCompanyRepository");

                //get all records
                Get["/", true] = async (x, ct) =>
                {
                    //Enable below to see an unhandled error in action
                    //throw new Exception("TEST EXCEPTION");

                    var obj = await _repo.GetAll();
                    return Response.AsJson(obj);
                };

                // Get single record
                Get["/{id:int}", true] = async (parameters, ct) =>
                {
                    Company obj = await _repo.GetById(parameters.id);
                    return Response.AsJson(obj);
                };

                // Search for record(s)
                Get["/search/{name}", true] = async (parameters, ct) =>
                {
                    var obj = await _repo.Find("Name LIKE @name", new { name = "%" + parameters.name + "%" });
                    return Response.AsJson(obj);
                };

                // Create a new record
                Post["/", true] = async (parameters, ct) =>
                {
                    var record = this.Bind<Company>();
                    var result = await _repo.Create(record);

                    return Response.AsJson(result);
                };

                // Update an existing record
                Put["/{id:int}", true] = async (parameters, ct) =>
                {
                    var record = this.Bind<Company>();
                    var result = await _repo.Update(record);

                    return Response.AsJson(result);
                };

                // Delete a record
                Delete["/{id:int}", true] = async (parameters, ct) =>
                {
                    int result = await _repo.SoftDelete(parameters.id);

                    return Response.AsJson(result);
                };
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }
    }
}