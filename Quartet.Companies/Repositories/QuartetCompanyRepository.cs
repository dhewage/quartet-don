﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib.Extensions;
using Microsoft.ApplicationInsights;
using Quartet.Companies.Interfaces;
using Quartet.Companies.Models;
using Quartet.Core.Interfaces;
using Quartet.Core.Models;

namespace Quartet.Companies.Repositories
{
    public class QuartetCompaniesAppSettings : IQuartetCompanyApplicationSettings
    {
        //Implementation of relevant application specific properties
        public string DBConnectionString { get; set; }

    }
    public class QuartetCompanyRepository : IQuartetDataRepository<Company>
    {
        private readonly string _connectionString;
        private readonly TelemetryClient _tc = new TelemetryClient();

        public QuartetCompanyRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task<long> Create(Company record)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                try
                {
                    var now = DateTime.UtcNow;
                    record.CreatedDate = now;
                    record.LastModified = now;
                    record.CreatedBy = 1;
                    record.LastModifiedBy = 1;

                    var result = await sqlConnection.InsertAsync(record);

                    return result;
                }
                catch (Exception ex)
                {
                    _tc.TrackException(ex);
                }

                return -1;
            }
        }

        public async Task<bool> Delete(Company record)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                try
                {
                    var result = await sqlConnection.DeleteAsync(record);

                    return result;
                }
                catch (Exception ex)
                {
                    _tc.TrackException(ex);
                }

                return false;
            }
        }

        public async Task<IEnumerable<Company>> Find(string sql, object param = null)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();
                try
                {
                    var records =
                        await
                            sqlConnection.QueryAsync<Company, DataDictionaryEntry, Company>(
                                string.Format(@"SELECT c.*, d.Id, d.Category, d.Name, d.ViewOrder 
                                    FROM companies.Companies c 
                                    INNER JOIN quartet.DataDictionaryEntries d ON c.CompanyTypeId = d.Id
                                    WHERE IsDeleted = 0 AND {0}", sql),
                                (c, d) =>
                                {
                                    c.CompanyType = d;
                                    return c;
                                },
                                param);

                    return records;
                }
                catch (Exception ex)
                {
                    _tc.TrackException(ex);
                }

                return null;
            }
        }

        public async Task<IEnumerable<Company>> GetAll()
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                try
                {
                    var records =
                        await sqlConnection.QueryAsync<Company, DataDictionaryEntry, Company>(
                            @"SELECT c.*, d.Id, d.Category, d.Name, d.ViewOrder 
                                    FROM companies.Companies c 
                                    INNER JOIN quartet.DataDictionaryEntries d ON c.CompanyTypeId = d.Id
                                    WHERE c.IsDeleted=0",
                            (c, d) =>
                            {
                                c.CompanyType = d;
                                return c;
                            }
                            );

                    return records;
                }
                catch (Exception ex)
                {
                    _tc.TrackException(ex);
                }

                return null;
            }
        }

        public async Task<Company> GetById(int tId)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                try
                {
                    var record =
                        await
                            sqlConnection.QueryAsync<Company, DataDictionaryEntry, Company>(
                                @"SELECT c.*, d.Id, d.Category, d.Name, d.ViewOrder 
                                    FROM companies.Companies c 
                                    INNER JOIN quartet.DataDictionaryEntries d ON c.CompanyTypeId = d.Id
                                    WHERE c.IsDeleted=0 AND c.Id=@Id",
                                (c, d) =>
                                {
                                    c.CompanyType = d;
                                    return c;
                                }, new { Id = tId }
                                );

                    return record.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    _tc.TrackException(ex);
                }

                return null;
            }
        }

        public async Task<int> SoftDelete(int Id)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                try
                {
                    var result =
                        await
                            sqlConnection.ExecuteAsync("UPDATE companies.Companies SET IsDeleted=1 WHERE Id=@id",
                                new { id = Id });

                    return result;
                }
                catch (Exception ex)
                {
                    _tc.TrackException(ex);
                }

                return -1;
            }
        }

        public async Task<bool> Update(Company record)
        {
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                try
                {
                    var originalRecord = sqlConnection.Get<Company>(record.Id);
                    record.CreatedDate = originalRecord.CreatedDate;
                    record.LastModified = DateTime.UtcNow;
                    record.CreatedBy = originalRecord.CreatedBy;
                    record.LastModifiedBy = 1;

                    var result = await sqlConnection.UpdateAsync(record);

                    return result;
                }
                catch (Exception ex)
                {
                    _tc.TrackException(ex);
                }

                return false;
            }
        }
    }
}