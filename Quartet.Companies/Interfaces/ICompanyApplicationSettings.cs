﻿
namespace Quartet.Companies.Interfaces
{
    public interface IQuartetCompanyApplicationSettings
    {
        // Property signatures such as connection string and company module's application specific properties can be defined below.
        string DBConnectionString { get; set; }
    }
}
