﻿using System;
using Dapper.Contrib.Extensions;

namespace Quartet.Companies.Models
{
    public interface ICompanyAddress
    {
        [Key]
        int Id { get; set; }

        int CustomerId { get; set; }
        bool IsDefault { get; set; }
        string Name { get; set; }
        string Street1 { get; set; }
        string Street2 { get; set; }
        string Street3 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Postcode { get; set; }
        int? CountryId { get; set; }
        bool IsDeleted { get; set; }
        DateTime CreatedDate { get; set; }
        int CreatedBy { get; set; }
        DateTime LastModified { get; set; }
        int LastModifiedBy { get; set; }
    }

    [Table("companies.CompanyAddresses")]
    public class CompanyAddress : ICompanyAddress
    {
        [Key]
        public int Id { get; set; }

        public int CustomerId { get; set; }
        public bool IsDefault { get; set; }
        public string Name { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string Street3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public int? CountryId { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime LastModified { get; set; }
        public int LastModifiedBy { get; set; }
    }
}