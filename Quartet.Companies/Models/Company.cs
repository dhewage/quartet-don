﻿using System;
using System.Security.Cryptography.X509Certificates;
using Dapper.Contrib.Extensions;
using Quartet.Core.Models;

namespace Quartet.Companies.Models
{
    public interface ICompany
    {
        [Key]
        int Id { get; set; }

        int RegionId { get; set; }
        int CompanyTypeId { get; set; }
        string Name { get; set; }
        bool IsEnabled { get; set; }
        DateTime CreatedDate { get; set; }
        int CreatedBy { get; set; }
        DateTime LastModified { get; set; }
        int LastModifiedBy { get; set; }
        DataDictionaryEntry CompanyType { get; set; }
    }

    [Table("companies.Companies")]
    public class Company : ICompany
    {
        [Key]
        public int Id { get; set; }

        public int RegionId { get; set; }
        public int CompanyTypeId { get; set; }
        public string Name { get; set; }
        public bool IsEnabled { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime LastModified { get; set; }
        public int LastModifiedBy { get; set; }
        public DataDictionaryEntry CompanyType { get; set; }
    }
}