﻿using Nancy.Hosting.Self;
using Quartet.Classes;
using System;

namespace Quartet
{
    public static class Program
    {
        public static void Main()
        {
            try
            {
                var listenUri = "http://localhost:1234";

                using (var host = new NancyHost(new Uri(listenUri)))
                {
                    host.Start();

                    Console.WriteLine("Quartet Server Started");
                    Console.WriteLine("Running in " +
                                      System.IO.Path.GetDirectoryName(
                                          System.Reflection.Assembly.GetExecutingAssembly().Location));
                    Console.WriteLine(" ");

                    Console.WriteLine("Listening on {0}", listenUri);

                    string s = "";
                    do
                    {
                        Console.Write("> ");
                        s = Console.ReadLine();
                        CommandProcessor.ProcessCommand(s);
                    } while (s.ToLower() != "quit");

                    host.Stop();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failure - {0}", ex.GetType());
            }
        }
    }
}