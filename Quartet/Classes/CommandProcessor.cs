﻿using System;

namespace Quartet.Classes
{
    public static class CommandProcessor
    {

        public static void ProcessCommand(string command)
        {
            switch (command.ToLower()){
                case "quit":
                    Console.WriteLine("Quitting the application...");
                    break;
                default:
                    Console.WriteLine("Received unknown command: " + command);
                    break;
            }
        }
    }
}
