﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nancy;

namespace Quartet.Classes
{
    public static class GlobalFunctions
    {
        //TODO: When adding a new API version the following must be incremented
        const int MaxVersion = 1;

        public static int GetRequestVersion(RequestHeaders headers)
        {
            int version = 1;

            var apiVersion = headers["X-API-VERSION"].FirstOrDefault();
            if (apiVersion != null)
            {
                int.TryParse(apiVersion, out version);
                if (version > MaxVersion)
                {
                    version = MaxVersion;
                }
            }

            return version;
        }
    }
}
