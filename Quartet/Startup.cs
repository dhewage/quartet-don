﻿ 
using System.Threading.Tasks;
using System.Web.Cors;
using Microsoft.Owin.Cors;
using Owin;
 
namespace Quartet
{
    public class Startup
    {
       

        private readonly CorsPolicy _corsPolicy = new CorsPolicy
        {
            AllowAnyHeader = true,
            AllowAnyMethod = false,
            AllowAnyOrigin = true,
            SupportsCredentials = true
        };
       

        private void ConfigureAuth()
        {
            _corsPolicy.ExposedHeaders.Add("X-API-VERSION");
            _corsPolicy.ExposedHeaders.Add("X-API-KEY");
            
            _corsPolicy.Methods.Add("GET");
            _corsPolicy.Methods.Add("POST");
            _corsPolicy.Methods.Add("PUT");
            _corsPolicy.Methods.Add("DELETE");

            // list of domains that are allowed can be added here
            // _corsPolicy.Origins.Add("domain");
            // be sure to include the port: "http://localhost:8081"
        }


        public void Configuration(IAppBuilder app)
        {
      

            ConfigureAuth();
          

            app.UseCors(new CorsOptions
            {
                PolicyProvider = new CorsPolicyProvider
                {
                    PolicyResolver = context => Task.FromResult(_corsPolicy)
                }
            });

            app.UseNancy();
        }



    }
}
