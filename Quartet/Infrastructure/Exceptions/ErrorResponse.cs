﻿using Nancy;
using Nancy.Bootstrapper;
using Nancy.Responses;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Net.Sockets;
using HttpStatusCode = Nancy.HttpStatusCode;

namespace Quartet.Infrastructure.Exceptions
{
    public class JsonSerializerConfiguration : IRegistrations
    {
        private readonly ICollection<TypeRegistration> _typeRegistrations;

        public JsonSerializerConfiguration()
        {
            _typeRegistrations = new Collection<TypeRegistration>
            {
                new TypeRegistration(typeof(ISerializer), typeof(DefaultJsonSerializer), Lifetime.Singleton)
            };
        }

        public IEnumerable<TypeRegistration> TypeRegistrations
        {
            get { return _typeRegistrations; }
        }

        public IEnumerable<CollectionTypeRegistration> CollectionTypeRegistrations
        {
            get { return null; }
        }

        public IEnumerable<InstanceRegistration> InstanceRegistrations
        {
            get { return null; }
        }
    }

    public class CustomJsonSerializer : JsonSerializer
    {
        public CustomJsonSerializer()
        {
            this.ContractResolver = new CamelCasePropertyNamesContractResolver();
        }
    }

    public class ErrorResponse : JsonResponse
    {
        private readonly Error error;

        private ErrorResponse(Error error) : base(error, new DefaultJsonSerializer())
        {
            this.error = error;
        }

        public string ErrorMessage { get { return error.ErrorMessage; } }
        public string FullException { get { return error.FullException; } }
        public string[] Errors { get { return error.Errors; } }

        public static ErrorResponse FromMessage(string message)
        {
            return new ErrorResponse(new Error { ErrorMessage = message });
        }

        public static ErrorResponse FromException(Exception ex)
        {
            var exception = ex;

            var summary = exception.Message;
            if (exception is WebException || exception is SocketException)
            {
                // Commonly returned when connections to DB fails
                summary = "The  windows service may not be running: " + summary;
            }

            const HttpStatusCode statusCode = Nancy.HttpStatusCode.InternalServerError;

            var error = new Error { ErrorMessage = summary, FullException = exception.ToString() };

            // Special cases
            /*if (exception is  ResourceNotFoundException)
            {
                statusCode = HttpStatusCode.NotFound;
                error.FullException = null;
            }

            if (exception is QuartetSecurityException)
            {
                statusCode = HttpStatusCode.Forbidden;
                error.FullException = null;
            }*/

            var response = new ErrorResponse(error);
            response.StatusCode = (Nancy.HttpStatusCode)statusCode;
            return response;
        }

        private class Error
        {
            public string ErrorMessage { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string FullException { get; set; }

            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string[] Errors { get; set; }
        }
    }
}