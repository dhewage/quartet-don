﻿using Nancy;
using Nancy.ErrorHandling;
using Nancy.Responses.Negotiation;
using System.Linq;

namespace Quartet.Infrastructure.Exceptions
{
    public sealed class ErrorStatusCodeHandler : IStatusCodeHandler
    {
        public bool HandlesStatusCode(HttpStatusCode statusCode, NancyContext context)
        {
            return statusCode == HttpStatusCode.NotFound
                || statusCode == HttpStatusCode.InternalServerError
                || statusCode == HttpStatusCode.Forbidden
                || statusCode == HttpStatusCode.Unauthorized;
        }

        public void Handle(HttpStatusCode statusCode, NancyContext context)
        {
            var clientWantsHtml = ShouldRenderFriendlyErrorPage(context);
            if (!clientWantsHtml)
            {
                if (context.Response is NotFoundResponse)
                {
                    context.Response = ErrorResponse.FromMessage("The resource you requested was not found.").WithStatusCode(statusCode);
                }
                // Pass the existing response through
                return;
            }

            var error = context.Response as ErrorResponse;
            switch (statusCode)
            {
                case HttpStatusCode.Unauthorized:
                    //redirect to loging page
                    break;

                case HttpStatusCode.Forbidden:
                    context.Response = new ErrorHtmlPageResponse(statusCode)
                    {
                        Title = "Permission",
                        Summary = error == null ? "Sorry, you do not have permission to perform that action. Please contact your Quartet administrator." : error.ErrorMessage
                    };
                    break;

                case HttpStatusCode.NotFound:
                    context.Response = new ErrorHtmlPageResponse(statusCode)
                    {
                        Title = "404 Not found",
                        Summary = "Sorry, the resource you requested was not found."
                    };
                    break;

                case HttpStatusCode.InternalServerError:
                    context.Response = new ErrorHtmlPageResponse(statusCode)
                    {
                        Title = "Sorry, something went wrong - 500",
                        Summary = error == null ? "An unexpected error occurred." : error.ErrorMessage,
                        Details = error?.FullException
                    };
                    break;
            }
        }

        private static bool ShouldRenderFriendlyErrorPage(NancyContext context)
        {
            var enumerable = context.Request.Headers.Accept;

            var ranges = enumerable.OrderByDescending(o => o.Item2).Select(o => MediaRange.FromString(o.Item1)).ToList();
            foreach (var item in ranges)
            {
                if (item.Matches("application/json"))
                    return false;
                if (item.Matches("text/json"))
                    return false;
                if (item.Matches("text/html"))
                    return true;
            }

            return true;
        }
    }
}