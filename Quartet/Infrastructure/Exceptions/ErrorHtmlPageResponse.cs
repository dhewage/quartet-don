﻿using Nancy.Responses;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Quartet.Infrastructure.Exceptions
{
    public class ErrorHtmlPageResponse : HtmlResponse
    {
        private static readonly Regex ReplacementTokenRegex = new Regex("\\#\\{(?<variable>.+?)\\}", RegexOptions.Compiled | RegexOptions.Singleline);
        private static readonly string ErrorTemplate;

        static ErrorHtmlPageResponse()
        {
            var stream = typeof(ErrorHtmlPageResponse).Assembly.GetManifestResourceStream("Quartet.Resources" + ".error.html");
            using (var reader = new StreamReader(stream))
            {
                ErrorTemplate = reader.ReadToEnd();
            }
        }

        public ErrorHtmlPageResponse(Nancy.HttpStatusCode statusCode)
        {
            StatusCode = statusCode;
            ContentType = "text/html; charset=utf-8";
            Contents = Render;
        }

        public string Title { get; set; }
        public string Summary { get; set; }
        public string Details { get; set; }

        private void Render(Stream stream)
        {
            var formatArguments = GetErrorPageDetails();

            string errorTitle = "";
            string errorSummary = "";
            string errorDetails = "";

            formatArguments.TryGetValue("ErrorTitle", out errorTitle);
            formatArguments.TryGetValue("ErrorSummary", out errorSummary);
            formatArguments.TryGetValue("ErrorDetails", out errorDetails);

            var page = ErrorTemplate.Replace("[Error.Title]", errorTitle).Replace("[Error.Summary]", errorSummary).Replace("[Error.Description]", errorDetails);

            if (errorSummary == null) page = page.Replace("[Error.Summary]", "");
            if (errorDetails == null) page = page.Replace("[Error.Description]", "");

            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine(page);
                writer.Flush();
            }
        }

        private Dictionary<string, string> GetErrorPageDetails()
        {
            var parameters = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
            parameters["ErrorTitle"] = Title;
            parameters["ErrorSummary"] = Summary;
            if (!string.IsNullOrWhiteSpace(Details))
            {
                parameters["ErrorDetails"] = "<h3>Details</h3><pre>" + Details + "</pre>";
            }
            return parameters;
        }
    }
}