﻿using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.ApplicationInsights.Extensibility;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Conventions;
using Nancy.TinyIoc;
using Quartet.Companies.Interfaces;
using Quartet.Companies.Modules;
using Quartet.Companies.Repositories;
using Quartet.Logger.Interfaces;
using System;
using System.Configuration;
using System.Diagnostics;

namespace Quartet
{
    public class ApplicationBootstrapper : DefaultNancyBootstrapper
    {
        private static readonly Stopwatch sw = new Stopwatch();
        private static readonly Logger.TelemetryClientLogger logger = new Logger.TelemetryClientLogger();

        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("/", "app"));
            base.ConfigureConventions(nancyConventions);
        }

        private static void InitializeApplicationInsights()
        {
            TelemetryConfiguration.Active.InstrumentationKey = ConfigurationManager.AppSettings["ApplicationInsights.InstrumentationKey"];
        }

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            try
            {
                InitializeApplicationInsights();
                base.ConfigureApplicationContainer(container);

                //Register dependencies for logger
                var logger = new Logger.TelemetryClientLogger();
                container.Register<ILogger>((c, p) => logger);

                logger.Info("Entered ConfigureApplicationContainer()");

                //Register dependencies
                var quartetAppSettings = new QuartetCompaniesAppSettings { DBConnectionString = ConfigurationManager.AppSettings["QUARTET_CONN_STRING"] };
                container.Register<IQuartetCompanyApplicationSettings>(quartetAppSettings);

                var companyModule = new CompanyModule(quartetAppSettings, logger);
                container.Register<CompanyModule>((ctr, param) => companyModule);

                //Resolve dependencies
                var quartetLoggerModule = container.Resolve<ILogger>();
                var quartetCompanyAppSettings = container.Resolve<IQuartetCompanyApplicationSettings>();
                var quartetCompanyModule = container.Resolve<CompanyModule>();

                logger.Info("Finished bootstrapping");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            try
            {
                // Hooking up filters for logging
                pipelines.BeforeRequest += ctx => PrepareLogging(context);
                pipelines.AfterRequest += ctx => StartLogging(context);

                //Centralised custom error handling
                pipelines.OnError.AddItemToEndOfPipeline((z, a) =>
                {
                    logger.Error(a);
                    return Infrastructure.Exceptions.ErrorResponse.FromException(a);
                });

                base.RequestStartup(container, pipelines, context);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        //Intercepting the requests via the request filter for logging
        private Response PrepareLogging(NancyContext ctx)
        {
            //Initialise logging
            sw.Start();
            var userName = "Anonymous";
            if (ctx.CurrentUser != null)
            {
                userName = ctx.CurrentUser.UserName;
            }

            return null;
        }

        // Intercepting responses via the response filter for logging
        private void StartLogging(NancyContext ctx)
        {
            //Prepare Telemetry object and track request
            sw.Stop();

            var requestTelemetry = new RequestTelemetry
            {
                Name = ctx.Request.Path,
                StartTime = DateTime.Now,
                Duration = sw.Elapsed,
                ResponseCode = ctx.Response.StatusCode.ToString(),
                Success = "OK" == ctx.Response.StatusCode.ToString(),
                Url = ctx.Request.Url,
                HttpMethod = ctx.Request.Method
            };

            //Make a call to Track request
            logger.TrackRequest(requestTelemetry);
        }
    }
}