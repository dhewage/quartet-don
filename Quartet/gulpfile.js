﻿var gulp = require('gulp');
var gutil = require('gulp-util');
var watch = require('gulp-watch');
var args = require('yargs').argv;

var minifycss = require('gulp-minify-css');
var sass = require('gulp-sass');
var imagemin = require('gulp-imagemin');
var uglify = require('gulp-uglify');

var outputPath = ".\\bin\\Debug\\";

gulp.task('default', ['setup', 'copyCSS', 'copyFonts', 'copyImages', 'copyCoreJS', 'copyJS', 'createCSS', 'copyHTML'], function () {
    gutil.log('Finished starting / running gulp');
});

gulp.task('setup', function() {
    outputPath = args.outputdir;

    gutil.log('File destination is ' + outputPath);
});

gulp.watch('html/css/**/*.scss', ['createCSS']);
gulp.watch('html/img/**/*', ['copyImages']);
gulp.watch('html/js/**/*.js', ['copyJS']);
gulp.watch('html/**/*.html', ['copyHTML']);

gulp.task('copyCSS', function () {
    gutil.log('Running task copyCSS');

    gulp.src('bower_components/bootstrap/dist/css/bootstrap-theme.min.css').pipe(gulp.dest(outputPath + 'app/assets/css/'));
    gulp.src('bower_components/bootstrap/dist/css/bootstrap.min.css').pipe(gulp.dest(outputPath + 'app/assets/css/'));

});

gulp.task('copyFonts', function () {
    gutil.log('Running task copyFonts');

    gulp.src('bower_components/bootstrap/dist/fonts/glyphicons-halflings-regular.*').pipe(gulp.dest(outputPath + 'app/assets/fonts/'));

});

gulp.task('copyImages', function () {
    gutil.log('Running task copyImages');

    return gulp.src('html/img/*')
		.pipe(imagemin({
		    progressive: true
		}))
		.pipe(gulp.dest(outputPath + 'app/assets/img'));

});

gulp.task('copyHTML', function () {
    gutil.log('Running task copyHTML');

    return gulp.src('html/*.html')
		.pipe(imagemin({
		    progressive: true
		}))
		.pipe(gulp.dest(outputPath + 'app/'));

});

gulp.task('copyCoreJS', function () {
    gutil.log('Running task copyCoreJS');

    gulp.src('bower_components/angular/angular.min.js').pipe(gulp.dest(outputPath + 'app/assets/js/'));
    gulp.src('bower_components/angular/angular.min.js.map').pipe(gulp.dest(outputPath + 'app/assets/js/'));
    gulp.src('bower_components/angular-messages/angular-messages.min.js').pipe(gulp.dest(outputPath + 'app/assets/js/'));
    gulp.src('bower_components/angular-messages/angular-messages.min.js.map').pipe(gulp.dest(outputPath + 'app/assets/js/'));
    gulp.src('bower_components/angular-resource/angular-resource.min.js').pipe(gulp.dest(outputPath + 'app/assets/js/'));
    gulp.src('bower_components/angular-resource/angular-resource.min.js.map').pipe(gulp.dest(outputPath + 'app/assets/js/'));

    gulp.src('bower_components/jquery/dist/jquery.min.js').pipe(gulp.dest(outputPath + 'app/assets/js/'));

    gulp.src('bower_components/bootstrap/dist/js/bootstrap.min.js').pipe(gulp.dest(outputPath + 'app/assets/js/'));

});

gulp.task('copyJS', function () {
    gutil.log('Running task copyJS');

    gulp.src('html/js/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(outputPath + 'app/assets/js/'));

});


gulp.task('createCSS', function () {
    gutil.log('Running task createCSS');

    return gulp.src('html/css/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(minifycss())
        .pipe(gulp.dest(outputPath + 'app/assets/css/'));

});