﻿using System;
using Nancy;
using Quartet.Classes;

namespace Quartet.Modules
{
    public class HomeModule : NancyModule 
    {
        private int _version = 1;

        public HomeModule() : base("/api")
        {

            Before += ctx =>
            {
                _version = GlobalFunctions.GetRequestVersion(Request.Headers);
                return null;
            };

            Get["/"] = _ => View["Index"];
        }
    }
}
