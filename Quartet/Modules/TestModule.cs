﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quartet.Modules
{
    public class TestModule : NancyModule
    {
        public TestModule() : base("/api/test")
        {
            Get["/"] = _ => "Hello from TestQ";
        }
    }
}
